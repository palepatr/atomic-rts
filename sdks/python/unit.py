class Unit:
    def __init__(self, attributes):
        self.id = attributes['id']
        self.player_id = attributes['player_id']
        self.x = attributes['x']
        self.y = attributes['y']
        self.type = attributes['type']
        self.status = attributes['status']
        self.health = attributes['health']
        self.attributes = attributes
        self.acting = False
        self.working = False

    def update(self, attributes):
        self.id = attributes['id']
        self.player_id = attributes['player_id']
        self.x = attributes['x']
        self.y = attributes['y']
        self.type = attributes['type']
        self.status = attributes['status']
        self.health = attributes['health']
        self.attributes = attributes

    def __str__(self):
        return str(self.attributes)


class Base(Unit):
    def __init__(self, attributes):
        super().__init__(attributes)
        self.resource = attributes['resource']

    def update(self, attributes):
        super().update(attributes)
        self.resource = attributes['resource']


class Worker(Unit):
    def __init__(self, attributes):
        super().__init__(attributes)
        self.can_attack = attributes['can_attack']
        self.can_carry = True
        self.range = attributes['range']
        self.speed = attributes['speed']
        self.resource = attributes['resource']
        self.attack_damage = attributes['attack_damage']
        self.attack_cooldown_duration = attributes['attack_cooldown_duration']
        self.attack_cooldown = attributes['attack_cooldown']
        self.attack_type = attributes['attack_type']

    def update(self, attributes):
        super().update(attributes)
        self.can_attack = attributes['can_attack']
        self.can_carry = True
        self.range = attributes['range']
        self.speed = attributes['speed']
        self.resource = attributes['resource']
        self.attack_damage = attributes['attack_damage']
        self.attack_cooldown_duration = attributes['attack_cooldown_duration']
        self.attack_cooldown = attributes['attack_cooldown']
        self.attack_type = attributes['attack_type']



class Scout(Unit):
    def __init__(self, attributes):
        super().__init__(attributes)
        self.can_attack = attributes['can_attack']
        self.range = attributes['range']
        self.speed = attributes['speed']
        self.attack_damage = attributes['attack_damage']
        self.attack_cooldown_duration = attributes['attack_cooldown_duration']
        self.attack_cooldown = attributes['attack_cooldown']
        self.attack_type = attributes['attack_type']

    def update(self, attributes):
        super().update(attributes)
        self.can_attack = attributes['can_attack']
        self.range = attributes['range']
        self.speed = attributes['speed']
        self.attack_damage = attributes['attack_damage']
        self.attack_cooldown_duration = attributes['attack_cooldown_duration']
        self.attack_cooldown = attributes['attack_cooldown']
        self.attack_type = attributes['attack_type']


class Tank(Unit):
    def __init__(self, attributes):
        super().__init__(attributes)
        self.can_attack = attributes['can_attack']
        self.range = attributes['range']
        self.speed = attributes['speed']
        self.attack_damage = attributes['attack_damage']
        self.attack_cooldown_duration = attributes['attack_cooldown_duration']
        self.attack_cooldown = attributes['attack_cooldown']
        self.attack_type = attributes['attack_type']

    def update(self, attributes):
        super().update(attributes)
        self.can_attack = attributes['can_attack']
        self.range = attributes['range']
        self.speed = attributes['speed']
        self.attack_damage = attributes['attack_damage']
        self.attack_cooldown_duration = attributes['attack_cooldown_duration']
        self.attack_cooldown = attributes['attack_cooldown']
        self.attack_type = attributes['attack_type']
