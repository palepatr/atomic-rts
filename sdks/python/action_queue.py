class ActionQueue:
    def __init__(self):
        self.actions = []

    def execute(self):
        if len(self.actions) > 0:
            a = self.actions[0]
            self.actions = self.actions[1:]
            return a
        return None

    def has_next(self):
        if len(self.actions) > 0:
            return True
        return False

    def clear(self):
        self.actions = []

    def add(self, action):
        self.actions.append(action)

    def add_many(self, actions):
        for a in actions:
            self.actions.append(a)

    def __str__(self):
        return str(self.actions)


def build_move_action(uid, dirs):
    moves = []
    move = "MOVE"
    if dirs == []:
        return moves
    for dir in dirs:
        c = {"command": move, "unit": uid, "dir": dir}
        moves.append(c)
    return moves


def build_gather_action(uid, direction):
    gather = 'GATHER'
    moves = []
    g = {"command": gather, "unit": uid, "dir": direction}
    moves.append(g)
    return moves


def build_shoot_action(uid, dx, dy):
    shoot = 'SHOOT'
    moves = []
    s = {"command": shoot, "unit": uid, "dx": dx, "dy": dy}
    moves.append(s)
    return moves
