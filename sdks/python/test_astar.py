from ai import a_star
from gamemap import GameMap


def make_map():
    tile = {'visible': True, 'x': 0, 'y': 0, 'blocked': False, 'resources': None, 'units': []}
    gm = []
    blocks = [(1,1),(1,2),(1,3),(2,1)]
    resources = [(2,2)]
    for x in range(4):
        for y in range(4):
            newtile = tile.copy()
            newtile['x'] = x
            newtile['y'] = y
            if (x,y) in blocks:
                newtile['blocked'] = True
            if (x,y) in resources:
                newtile['resources'] = 'blah'
            gm.append(newtile)
    return gm





if __name__ == '__main__':
    gm = GameMap(make_map())
    start = (0,0)
    goal = (2,2)
    pathmap = a_star(start, goal, gm)
    print(pathmap)
    start = (2,2)
    goal = (0,0)
    pathmap = a_star(start, goal, gm)
    print(pathmap)

