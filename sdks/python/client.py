    #!/usr/bin/python

import sys
import json
import random
import ai

if (sys.version_info > (3, 0)):
    print("Python 3.X detected")
    import socketserver as ss
else:
    print("Python 2.X detected")
    import SocketServer as ss


class NetworkHandler(ss.StreamRequestHandler):
    def handle(self):
        game = Game()

        while True:
            try:
                data = self.rfile.readline().decode() # reads until '\n' encountered
                json_data = json.loads(str(data))
            except:
                print('Error reading json_data, exiting...')
                exit(1)
            # uncomment the following line to see pretty-printed data
            # print(json.dumps(json_data, indent=4, sort_keys=True))
            # try:
            #     response = game.create_response(json_data)
            #     self.wfile.write(response)
            # except:
            #     print('Error building response, exiting...')
            #     exit(1)
            response = game.create_response(json_data)
            self.wfile.write(response)



class Game:
    def __init__(self):
        # self.units = set() # set of unique unit ids
        self.units = {}
        self.directions = ['N', 'S', 'E', 'W']
        self.ai = ai.AI(self.units, self.directions)

    def create_response(self, json_data):
        actions = self.ai.act(json_data)
        response = json.dumps(actions, separators=(',',':')) + '\n'
        return response.encode()


if __name__ == "__main__":
    port = int(sys.argv[1]) if (len(sys.argv) > 1 and sys.argv[1]) else 9090
    host = '0.0.0.0'

    server = ss.TCPServer((host, port), NetworkHandler)
    print("listening on {}:{}".format(host, port))
    try:
        server.serve_forever()
    except:
        print('Error with NetworkHandler, exiting...')
        exit(1)
