def manhattan(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


# start and goal are lists of format [x,y]
def a_star(start, goal, gmap):
    openset = set()
    openset.add(start)
    closedset = set()
    g_cost = {}
    g_cost[start] = 0
    f_cost = {}
    f_cost[start] = manhattan(start[0], start[1], goal[0], goal[1])
    came_from = {}

    while len(openset) > 0:
        temp = openset.copy()
        min = temp.pop()
        for k in openset:
            if k in f_cost and f_cost[k] < f_cost[min]:
                min = k
        cur = min
        if cur == goal:
            return make_path(came_from, goal)
        openset.remove(cur)
        closedset.add(cur)

        for neighbor in gmap.neighbors(cur[0], cur[1]):
            # print('cur:', cur, 'neighbor:', neighbor)
            # print('g_cost:', g_cost)
            if neighbor in closedset:
                continue
            if neighbor not in openset:
                openset.add(neighbor)

            g = g_cost[cur] + 1
            # print('g:', g, 'g_cur:', g_cost[cur])
            if neighbor in g_cost.keys():
                n_g = g_cost[neighbor]
            else:
                n_g = (g_cost[cur] + 1) * 100
            if g >= n_g:
                continue
            came_from[neighbor] = cur
            g_cost[neighbor] = g
            f_cost[neighbor] = g_cost[neighbor] + manhattan(neighbor[0], neighbor[1], goal[0], goal[1])

    return make_path(came_from, goal)


def make_path(came_from, current):
    final_path = [current]
    while current in came_from.keys():
        old = current
        current = came_from[current]
        final_path.append(current)
        came_from.pop(old)

    #return final_path[::-1]
    return path_to_dir(final_path[::-1])


def path_to_dir(path):
    directions = []
    for i, p in enumerate(path[:-1]):
        start, next = path[i], path[i+1]
        xdif = next[0] - start[0]
        ydif = next[1] - start[1]
        if xdif == 1:
            directions.append('E')
        elif xdif == -1:
            directions.append('W')
        elif ydif == 1:
            directions.append('S')
        elif ydif == -1:
            directions.append('N')
    return directions


def reverse_path(directions):
    dirs = []
    for d in directions:
        dirs.append(d['dir'])
    opp = {'N': 'S', 'S': 'N', 'E': 'W', 'W': 'E'}
    rev_dirs = dirs[::-1]
    rev_path = []
    for dir in rev_dirs:
        rev_path.append(opp[dir])
    # print(rev_path)
    new_directions = []
    for i, d in enumerate(directions):
        cmd = d.copy()
        cmd['dir'] = rev_path[i]
        new_directions.append(cmd)
    return new_directions