import random
from unit import Unit, Base, Worker, Scout, Tank
from gamemap import GameMap
from astar import a_star, reverse_path
from action_queue import ActionQueue, build_move_action, build_gather_action, build_shoot_action


class AI:
    def __init__(self, units, directions):
        self.game_state = None
        self.gmap = None
        self.units = units
        self.a_queues = {}
        self.directions = directions
        self.game_info = None
        self.resources = 0
        self.scouts = {}

    def act(self, game_state):
        self.update_state(game_state)
        commands = {"commands": []}

        purchases = self.spend_resources()
        for c in purchases:
            commands['commands'].append(c)

        # for cmd in self.random_move():
        #     commands["commands"].append(cmd)

        # for id in self.units:
        for id in self.units:
            has_next =  self.a_queues[id].has_next()
            status = self.units[id].status
            acting = self.units[id].acting
            # print('has_next:', has_next, 'status:', status, 'acting:', acting)
            if has_next and status == 'idle':
                commands['commands'].append(self.a_queues[id].execute())
            elif not has_next and status == 'idle':
                self.units[id].acting = False
            if not acting:
                self.assign_work(id)
                # print(self.a_queues[id])
        # print(commands)
        return commands

    def update_state(self, game_state):
        self.game_state = game_state
        state = game_state
        # units = set([unit['id'] for unit in state['unit_updates'] if unit['type'] != 'base'])
        self.update_units(state['unit_updates'])
        if self.game_info is None:
            self.game_info = state['game_info']
        if self.gmap is None:
            self.gmap = GameMap(state['tile_updates'])
        else:
            self.gmap.update(state['tile_updates'])

    def update_units(self, unit_updates):
        for unit in unit_updates:
            if unit['id'] == 6:
                # print(unit['status'])
                pass
            if unit['type'] == 'base':
                u = Base(unit)
                self.resources = u.resource
            elif unit['type'] == 'worker':
                u = Worker(unit)
            elif unit['type'] == 'scout':
                u = Scout(unit)
            elif unit['type'] == 'tank':
                u = Tank(unit)
            if u.id in self.units.keys():
                self.units[u.id].update(unit)
            else:
                self.units[u.id] = u
            if u.id not in self.a_queues.keys():
                self.a_queues[u.id] = ActionQueue()

    def get_all_units(self):
        units = [unit for id, unit in self.units.items()]
        return units

    def get_all_workers(self):
        units = [unit for id, unit in self.units.items() if unit.type == 'worker']
        return units

    def get_all_scouts(self):
        units = [unit for id, unit in self.units.items() if unit.type == 'scout']
        return units

    def get_all_tanks(self):
        units = [unit for id, unit in self.units.items() if unit.type == 'tanks']
        return units

    def get_units_no_bases(self):
        units = [unit for id, unit in self.units.items() if unit.type != 'base']
        return units

    def create_unit(self, type):
        create = 'CREATE'
        if type in ['worker', 'scout', 'tank']:
            command = {"command": create, "type": type}
            return command
        return None

    def random_move(self):
        units = self.get_all_workers()
        if len(units) > 0:
            unit = random.choice(units)
        else:
            return False
        direction = random.choice(self.directions)
        move = 'MOVE'
        self.a_queues[unit.id].add({"command": move, "unit": unit.id, "dir": direction})
        return True

    def random_move_one(self, unit):
        direction = random.choice(self.directions)
        move = 'MOVE'
        self.a_queues[unit.id].add({"command": move, "unit": unit.id, "dir": direction})
        return True

    def move(self, unit, x, y):
        u = self.units[unit.id]
        start = (u.x, u.y)
        goal = (x, y)
        if self.gmap.node(x, y):
            path = a_star(start, goal, self.gmap)
            return build_move_action(u.id, path)
        else:
            return None

    def path(self, unit_id, start, goal):
        if self.gmap.node(goal[0], goal[1]):
            path = a_star(start, goal, self.gmap)
            return build_move_action(unit_id, path)
        else:
            return None

    def in_range(self, unit, target):
        range = unit.range
        x, y, tx, ty = unit.x, unit.y, target[0], target[1]
        if abs(tx - x) <= range and abs(ty - y) <= range:
            return True
        return False

    def gather(self, unit):
        # print('GATHER ACTION')
        close = self.gmap.get_closest(unit, 'resource')
        if close is not None:
            move_actions = self.move(unit, close.x, close.y)
            if len(move_actions) > 0:   # valid move
                # print('closest resource:', close.x, close.y)

                gather_dir = move_actions[-1]['dir']
                move_actions = move_actions[:-1]
                move_goal = self.gmap.parse_destination((unit.x, unit.y), move_actions)
                home_moves = self.path(unit.id, move_goal, (0, 0))
                gather_actions = build_gather_action(unit.id, gather_dir)
                if not self.units[unit.id].working:
                    self.a_queues[unit.id].clear()
                self.a_queues[unit.id].add_many(move_actions)
                self.a_queues[unit.id].add_many(gather_actions)
                self.a_queues[unit.id].add_many(home_moves)

                return True
        return False

    def scout(self, unit, direction):
        goals = {'N': self.gmap.farN, 'S': self.gmap.farS, 'E': self.gmap.farE, 'W': self.gmap.farW,
                 'NW': self.gmap.farNW, 'NE': self.gmap.farNE, 'SW': self.gmap.farSW, 'SE': self.gmap.farSE}
        goal = goals[direction]
        start = (unit.x, unit.y)
        scout_actions = self.move(unit, goal[0], goal[1])
        if len(scout_actions) > 1:
            self.a_queues[unit.id].add_many(scout_actions)
            return True
        return False

    def attack(self, unit, x, y):
        if unit.type == 'tank':
            dx = x - unit.x
            dy = y - unit.y
            shoot_action = build_shoot_action(unit.id, dx, dy)
            self.a_queues[unit.id].add_many(shoot_action)
            return True
        return False


    def assign_work(self, id):
        unit = self.units[id]
        if unit.type == 'worker':
            # print('ASSIGNING WORK TO:', id)
            self.units[id].acting = True
            if not self.gather(unit):
                # print('could not assign work for:', unit.id)
                self.a_queues[id].clear()
                self.random_move_one(unit)
                self.units[id].working = False
                self.units[id].acting = False
            else:
                self.units[id].working = True

        if unit.type == 'scout':
            # print('Scouts Directions:', self.scouts)
            # print('Scouts:', str([u.id for u in self.get_all_scouts()]))
            self.units[id].acting = True
            if id not in self.scouts.keys():
                direction = random.choice(self.directions + ['NW','NE','SW','SE'])
            else:
                direction = self.scouts[id]
            self.scouts[id] = direction
            if not self.scout(unit, direction):
                self.scouts.pop(id)
                self.a_queues[id].clear()
                self.random_move_one(unit)
                self.units[id].working = False
                self.units[id].acting = False
            else:
                self.units[id].working = True

        if unit.type == 'tank':
            self.units[id].acting = True
            if self.gmap.base_found:
                if self.in_range(unit, self.gmap.enemy_base_loc):
                    self.a_queues[id].clear()
                    self.attack(unit, self.gmap.enemy_base_loc[0], self.gmap.enemy_base_loc[1])
                else:
                    move_actions = self.move(unit, self.gmap.enemy_base_loc[0], self.gmap.enemy_base_loc[1])
                    if len(move_actions) > 0:
                        self.a_queues[id].add_many(move_actions)
                self.units[id].working = True
            else:
                self.random_move_one(unit)
                self.units[id].working = False
                self.units[id].acting = False

    def spend_resources(self):
        costs = {
            'worker': 100,
            'scout': 130,
            'tank': 150
        }
        num_workers = len(self.get_all_workers())
        num_scouts = len(self.get_all_scouts())
        num_tanks = len(self.get_all_tanks())
        base_found = self.gmap.base_found
        money = self.resources

        commands = []

        if num_scouts < 2 and money >= 460:
            commands.append(self.create_unit('scout'))
            commands.append(self.create_unit('scout'))

        if num_scouts in range(1,3) and money > 460:
            commands.append(self.create_unit('scout'))

        if money > 700 and not base_found:
            commands.append(self.create_unit('scout'))

        if money >= 1000:
            commands.append(self.create_unit('worker'))

        if base_found and num_tanks < 3:
            commands.append(self.create_unit('tank'))

        return commands
