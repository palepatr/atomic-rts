from astar import a_star


class GameMap:
    def __init__(self, tiles):
        self.graph = {}
        # points furthest from the base in every direction
        self.farN = (0, 0)
        self.farNE = (0, 0)
        self.farE = (0, 0)
        self.farSE = (0, 0)
        self.farS = (0, 0)
        self.farSW = (0, 0)
        self.farW = (0, 0)
        self.farNW = (0, 0)
        self.furthest = (0, 0)
        self.base_found = False
        self.enemy_base_loc = None

        self.update(tiles)

    def update(self, tiles):
        for tile in tiles:
            x, y = tile['x'], tile['y']
            key = (x, y)
            if tile['visible'] is True:
                node = Node(x, y, tile['visible'], tile['blocked'], tile['resources'])
                self.graph[key] = node
                if len(tile['units']) > 0:
                    for u in tile['units']:
                        if u['type'] == 'base':
                            self.base_found = True
                            self.enemy_base_loc = key
                            print(key)
            else:
                self.graph[key].visible = tile['visible']
        self.set_furthests()

    def node(self, x, y):
        key = (x, y)
        if key in self.graph.keys():
            return self.graph[key]
        return None

    def neighbors(self, x, y):
        key = (x, y)
        dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]]
        result = []
        for dir in dirs:
            neighbor = (self.graph[key].x + dir[0], self.graph[key].y + dir[1])
            nkey = (neighbor[0], neighbor[1])
            if nkey in self.graph.keys():
                nbor = self.graph[nkey]
                if nbor.blocked is False or nbor.resources is not None:
                    result.append((nbor.x, nbor.y))
        return result

    def set_furthests(self):
        for key, node in self.graph.items():
            if node.blocked:
                continue
            x, y = key[0], key[1]
            dist = abs(key[0]) + abs(key[1])
            if x == 0:
                if y < 0 and dist > self.distance_from_base(self.farN):
                    self.farN = key
                elif y > 0 and dist > self.distance_from_base(self.farS):
                    self.farS = key
            elif y == 0:
                if x < 0 and dist > self.distance_from_base(self.farW):
                    self.farW = key
                elif x > 0 and dist > self.distance_from_base(self.farE):
                    self.farE = key
            elif x > 0 and y > 0 and dist > self.distance_from_base(self.farSE):
                self.farSE = key
            elif x > 0 and y < 0 and dist > self.distance_from_base(self.farNE):
                self.farNE = key
            elif x < 0 and y > 0 and dist > self.distance_from_base(self.farSW):
                self.farSW = key
            elif x < 0 and y < 0 and dist > self.distance_from_base(self.farNW):
                self.farNW = key

        self.set_furthest_point()

    def set_furthest_point(self):
        furthest, dist = self.furthest, self.distance_from_base(self.furthest)
        points = [self.farN, self.farNE, self.farNW, self.farE, self.farS, self.farSE, self.farSW, self.farW]
        for point in points:
            d = self.distance_from_base(point)
            if d > dist:
                furthest = point
                dist = d
        self.furthest = furthest

    def distance_from_base(self, point):
        dist = abs(point[0]) + abs(point[1])
        return dist

    def distance(self, p1, p2):
        return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

    def get_all_resource_nodes(self):
        resource_nodes = []
        for i, node in self.graph.items():
            if node.resources:
                resource_nodes.append(node)
        return resource_nodes

    def get_closest(self, unit, target):
        x, y = unit.x, unit.y
        if target == 'resource':    # going to get closest resources
            resources = self.get_all_resource_nodes()
            if len(resources) > 0:
                min = self.distance((0, 0), (resources[0].x, resources[0].y))
                closest = resources[0]
                # print('Close resources:')
                # print('-------------------')
                for r in resources:
                    start, goal = (x, y), (r.x, r.y)
                    # d = self.distance((0, 0), (r.x, r.y))
                    d = len(a_star(start, goal, self))
                    if d == 0:
                        continue
                    # print('({},{}), d: {}, current min: {}'.format(r.x, r.y, d, min))
                    if d < min:
                        min = d
                        closest = r
                return closest
            return None

    def parse_destination(self, start, commands):
        dirs = []
        x, y = start[0], start[1]
        for cmd in commands:
            dirs.append(cmd['dir'])

        for d in dirs:
            if d == 'N':
                y -= 1
            elif d == 'S':
                y += 1
            elif d == 'E':
                x += 1
            elif d == 'W':
                x -= 1

        return (x, y)



class Node:
    def __init__(self, x, y, visible, blocked, resources):
        self.x = x
        self.y = y
        self.visible = visible
        self.blocked = blocked
        self.resources = resources
        self.cost_to_base = 0
